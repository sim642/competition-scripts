#!/usr/bin/env python3

import sys
import argparse
import httpx
import json


def get_json_from_zenodo(doi):
    record_id = doi.replace("10.5281/zenodo.", "")
    client = httpx.Client(http2=True)
    response = client.get(f"https://zenodo.org/api/records/{record_id}")
    if response.status_code != 200:
        print(f"Error: Bad status code: {response.status_code}")
        exit(1)
    return json.loads(response.content)


def parser():
    p = argparse.ArgumentParser(
        description="Return download URL for a given Zenodo DOI"
    )
    p.add_argument(
        "DOI",
        nargs=1,
        type=str,
        default="",
        help="DOI to get a download URL for",
    )
    p.add_argument(
        "--print-zenodo-json",
        dest="print_zenodo_json",
        action="store_true",
        default=False,
        help="Print the JSON export of the Zenodo record instead",
    )
    return p


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args = parser().parse_args(argv)

    content = get_json_from_zenodo(args.DOI[0])
    if args.print_zenodo_json:
        print(json.dumps(content, indent=4))
    else:
        print(content["files"][0]["links"]["self"])


if __name__ == "__main__":
    sys.exit(main())
